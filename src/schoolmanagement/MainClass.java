package schoolmanagement;

import java.util.Random;

import schoolmanagement.School;
import schoolmanagement.Student;
import schoolmanagement.University;

public class MainClass {
	
	public static void main(String[] args) 
	{
		School school1 = new School("0001","Hristo Botev's school","bul.slivnica 995");
		School school2 = new School("0002","Vasil Levski's school","bul.vladislav varnenchik 3");
		School school3 = new School("0003","Georgi Rakovski's school","bul.treti mart 15");
		University university1=new University("0004","Tehicheski university","ul.Studentska 1");
		University university2=new University("0005","Ikonomicheski university","bul.Slivnica 165");
		
		String[] studentNamesMale= {"Martin","Todor","Hrelio","Delio","Svilen"};
		String[] studentNamesFemale= {"Borislava","Desislava","Kalia","Yuliana","Snezha"};

		Facilities[] facilities= {school1,school2,school3,university1,university2}; 
		
		for(int i=0;i<50;i++)
		{
			Random r = new Random();
			int randomNameIndex=r.nextInt(studentNamesMale.length);
			int randomAge=r.nextInt((65 - 20) + 1) + 20;
			boolean isMale=r.nextBoolean();
			
			Student obj;
			if(isMale) 
				 obj = new Student(studentNamesMale[randomNameIndex],randomAge,"male");
			else 
				 obj = new Student(studentNamesFemale[randomNameIndex],randomAge,"female");
			
			facilities[i/10].addStudent(obj);
		}
		
		printCertainFacilitysCalculations(school1);	
		System.out.println("#######################################################");
		printFacilitiesTotalIncome(facilities);
		System.out.println("#######################################################");
		printFacilitiesTopContributors(facilities);
	}
	
	public static void printCertainFacilitysCalculations(Facilities x)
	{
		x.printAvgGradeAll();
		x.printFacilityAvg(); 
		x.printBestStudent();
		x.printBestMaleStudent();
		x.printBestFemaleStudent();
	}
	
	public static void printFacilitiesTotalIncome(Facilities[] x)
	{	
		for(int i=0;i<x.length;i++)
		{
			x[i].printTotalIncome();
		}
	}
	
	public static void printFacilitiesTopContributors(Facilities[] x)
	{
		for(int i=0;i<x.length;i++)
		{
			x[i].printTopContributor();
		}
	}
}
