package schoolmanagement;

import java.util.Random;

public class Student {
	String name;
	String gender;
	String entityId;
	int age;
	double avg_grade;
    Facilities studiesAt;
    
	public Student() 
	{
		this.name="default";
		this.age=0;
		this.gender="default";
		avg_grade=0;
	}
	
	public Student(String name,int age,String gender) 
	{
		this.name=name;
		this.age=age;
		this.gender=gender;
		avg_grade=Math.round(genAvgGrade() * 100d) / 100d; // the random number is rounded to 2 decimal places
	}
	
	private double genAvgGrade() //generating random number
	{
		Random r = new Random();
		return 2 + (6 - 2) * r.nextDouble();
	}
	
	public void setStudiesAt(Facilities o)
	{
		studiesAt=o;
	}
	
	public void setEntityId(String entityId)
	{
		this.entityId=entityId;
	}
	
	public double getAvgGrade() 
	{
		return avg_grade;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public double calculatePayment()
	{
		return (age/studiesAt.getFacilityAvg())*100+studiesAt.getTax();
	}
}
