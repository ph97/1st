package schoolmanagement;

import java.util.ArrayList;

import schoolmanagement.Student;

public abstract class Facilities 
{
	String id,name,address;
	ArrayList<Student>students=new ArrayList<>();
	double facilityAvg;
	double totalIncome;
	int facilityTax;
	Student topContributor;
	Student best=new Student();                       //sets the objects to be initialized with default constructor 
	Student bestMale=new Student();					  //in order to avoid null values when comparing their average grades
	Student bestFemale=new Student();				  //later on , in the loop below
	
	public Facilities(String id,String name,String address,int tax) 
	{
		this.id=id;
		this.name=name;
		this.address=address;
		this.facilityTax=tax;
	}

	public void addStudent(Student o)
	{
		System.out.println("Hello "+o.getName()+"("+o.getAge()+" years old) and welcome to "+ name);
		System.out.println("----------------------------------------------------------------------------");
		
		students.add(o);                         //adds the student to the school's array(db)
		o.setEntityId(id);                       //sets the student's id to be = facility's id
		o.setStudiesAt(this);                    //sets where the student studies (to the current object)
		
		calcFacilityAvg();                       //calculates the current facility's average
		facilitysCalculations();				 //calculates the current facility's total income,top contributor,best student,best male,best female
	}
	
	private void calcFacilityAvg()
	{
		double temp=0;
		for(int i=0;i<students.size();i++)
			temp+=students.get(i).getAvgGrade();
		
		facilityAvg=temp/students.size();
	}
	
	private void facilitysCalculations()
	{
		for(int i=0;i<students.size();i++)                                             //loops through the students in the current facility(to find things)
		{
			topContributor=students.get(0);                                            //sets the start of topContributor to be the first student
			
			if(students.get(i).calculatePayment()>topContributor.calculatePayment())   //finds the top contributor by comparing who paid more
				topContributor=students.get(i);
			
			if(students.get(i).getAvgGrade() > best.getAvgGrade())                     //finds the best student by comparing their average grades
				best=students.get(i);
			
			if(students.get(i).getGender()=="male")                                    //finds the best male student
			{
				if(students.get(i).getAvgGrade() > bestMale.getAvgGrade())             //finds the best male by comparing the average grade
				{
					bestMale=students.get(i);
				}
			}
			else if(students.get(i).getGender()=="female")                             //finds the best female student
			{
				if(students.get(i).getAvgGrade() > bestFemale.getAvgGrade())           //finds the best female by comparing the average grade
				{
					bestFemale=students.get(i);
				}
			}
			
			totalIncome+=students.get(i).calculatePayment();                           //finds the total income for the current facility
		}
	}
	
	public int getTax()
	{
		return facilityTax;
	}
	
	public double getFacilityAvg()
	{
		return facilityAvg;
	}
	
	public void printTotalIncome()
	{
		System.out.println("total income for "+name +":"+ Math.round(totalIncome * 100d) / 100d);
	}
	
	public void printFacilityAvg()
	{
		System.out.println("************************************************************");
		System.out.println("facility's average for "+name+":"+ Math.round(facilityAvg * 100d) / 100d);
		System.out.println("************************************************************");
	}
	
	public void printBestStudent()
	{
		System.out.println("Best student for "+name+":"+best.getName()+", with average grade of "+best.getAvgGrade());
	}
	
	public void printBestMaleStudent()
	{
		System.out.println("Best male student for "+name+":"+bestMale.getName()+", with grade of "+bestMale.getAvgGrade());
	}
	
	public void printBestFemaleStudent()
	{
		System.out.println("Best female student for "+name+":"+bestFemale.getName()+", with grade of "+bestFemale.getAvgGrade());
	}
	
	public void printAvgGradeAll()
	{
		System.out.println("Each student's average grade for "+name+":");
		for(int i=0;i<students.size();i++) 
		{
			System.out.println("Student:"+students.get(i).getName()+" ,average grade:"+students.get(i).getAvgGrade());
		}
	}
	
	public void printTopContributor()
	{
		System.out.println("_______________________________________________________________________________________");
		System.out.println("Top contributor for "+name+" is "+topContributor.getName()+" with tax of "+Math.round( topContributor.calculatePayment() * 100d) / 100d);
	}
	
}
